import axios from "axios";

export const APIRequest = axios.create({
  baseURL: "https://run.mocky.io/v3/",
});
